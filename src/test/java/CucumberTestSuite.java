import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import net.serenitybdd.junit.runners.SerenityRunner;
import org.junit.runner.RunWith;

/**
 * <p>Title: CucumberTestSuite</p>
 * <p>Description: </p>
 *
 * @author yu.chen
 * @date 2021/6/2 2:04 PM
 * ------------------- History -------------------
 * <date>      <author>       <desc>
 * 2021/6/2   yu.chen    初始创建
 * -----------------------------------------------
 */

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty" }, features = "src/test/resources/features/login/LoginPage.feature")
public class CucumberTestSuite {

}